package yjhestem.ntnu.mobilewearprogramming.moviesearch;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Bruker on 04.03.2018.
 *
 * This store information about a movies in a list, which is different than Movie
 */

public class MovieInList {

    private int id;
    private String name;

    public MovieInList(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public MovieInList(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
