package yjhestem.ntnu.mobilewearprogramming.moviesearch;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MovieInfo extends AppCompatActivity {
    String TAG = "MovieInfo";

    // Access a Cloud Firestore instance from your Activity
    FirebaseFirestore db;
    FirebaseAuth dbAuth;
    private String movieId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_info);

        Intent intent = getIntent();
        movieId = intent.getStringExtra("movieId");

        // Get user-prefferred language:
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final String language = prefs.getString("language",MainActivity.defaultLanguage);
        Log.d(TAG, "onCreate: Starting to get json.");
        String url = "https://api.themoviedb.org/3/movie/" + movieId + "?api_key=" + MainActivity.apiKeyTMDB + "&language=" + language;
        new GetJSON(this,url,"movie").execute();        // Get movie-info and show them.
        Log.d(TAG, "onCreate: Started GetJSON");
        db = FirebaseFirestore.getInstance();
        dbAuth = FirebaseAuth.getInstance();

        checkFavorite();            // Check if a user is favorited or not.

        // Listen for new comments
        db.collection("moviesTMDB").document(movieId).collection("comments").orderBy("messageTime")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        List<ChatMessage> comments = new ArrayList<ChatMessage>();
                        for (QueryDocumentSnapshot document : value) {
                            Log.d(TAG, document.getId() + " => " + document.getData());
                            ChatMessage comment = document.toObject(ChatMessage.class);
                            comments.add(comment);
                        }
                        new ShowChatMessage(MovieInfo.this,comments).execute();
                    }
                });
    }

    public void sendComment(View view) {
        // Get comment:
        EditText text = (EditText)findViewById(R.id.et_text);

        // Create a new comment:
        ChatMessage comment = new ChatMessage(text.getText().toString(), dbAuth.getCurrentUser().getDisplayName(), dbAuth.getCurrentUser().getUid());
        text.setText("");       // "Delete" message from editText so it's ready for a new one.

        // Add a new document with a generated ID
        db.collection("moviesTMDB").document(movieId).collection("comments")
                .add(comment)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "Comment to " + movieId + " added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding comment", e);
                    }
                });

    }

    // Add favorite:
    public void favorite(View view) {
        //Make addable item with generated id:
        Favorite data = new Favorite();
        data.setFavoriteId(movieId);
        data.setUserId(dbAuth.getCurrentUser().getUid());
        data.setUsername(dbAuth.getCurrentUser().getDisplayName());

        // Add a new document with a generated ID
        db.collection("user").document(dbAuth.getCurrentUser().getUid()).collection("favorites_moviesTMDB")
                .add(data)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "User " + dbAuth.getCurrentUser().getDisplayName()  + " has set " + movieId + " as favorite (added with ID: " + documentReference.getId() + ")");
                        Button bt_favorite = (Button) findViewById(R.id.bt_favorite);
                        bt_favorite.setText("Added as favorite");           // When added as favorite, write it and set button to not-clickable.
                        bt_favorite.setEnabled(false);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding as favorite", e);
                    }
                });

        db.collection("moviesTMDB").document(movieId).collection("favorites")
                .add(data)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "User " + dbAuth.getCurrentUser().getDisplayName()  + " has set " + movieId + " as favorite (added with ID: " + documentReference.getId() + ")");
                        Button bt_favorite = (Button) findViewById(R.id.bt_favorite);
                        bt_favorite.setText("Added as favorite");
                        bt_favorite.setEnabled(false);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding as favorite", e);
                    }
                });
    }


    public void seePreviews(View view) {
        TextView title = (TextView) findViewById(R.id.tv_title);

        Intent intent = new Intent(this, PreviewList.class);
        Log.d(TAG, "On item click: Movie to show previews for (id): " + movieId);
        Log.d(TAG, "On item click: Movie to show previews for (title): " + title.getText().toString());
        intent.putExtra("movieId", movieId);
        intent.putExtra("movieTitle", title.getText().toString());
        startActivity(intent);                                          // Go to intent.
    }

    // Check if the user has favorited it.
    public void checkFavorite() {
        db.collection("user").document(dbAuth.getCurrentUser().getUid()).collection("favorites_moviesTMDB")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Favorite favorite = document.toObject(Favorite.class);
                                if (Objects.equals(favorite.getFavoriteId(), movieId)) {
                                    Button bt_favorite = (Button) findViewById(R.id.bt_favorite);
                                    bt_favorite.setText("Added as favorite");
                                    bt_favorite.setEnabled(false);
                                }
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }
}
