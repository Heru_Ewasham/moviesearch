package yjhestem.ntnu.mobilewearprogramming.moviesearch;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.uwetrottmann.thetvdb.entities.Series;
import com.uwetrottmann.thetvdb.entities.SeriesResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TVseriesInfo extends AppCompatActivity {
    String TAG = "TVseriesInfo";

    // Access a Cloud Firestore instance from your Activity
    FirebaseFirestore db;
    FirebaseAuth dbAuth;
    private String tvSeriesId;
    private String tvSeriesProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tvseries_info);

        Intent intent = getIntent();
        tvSeriesId = intent.getStringExtra("tvSeriesId");
        tvSeriesProvider = intent.getStringExtra("tvSeriesProvider");       // Is it TMDB or TVDB

        //Get language-preference:
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final String language = prefs.getString("language","en");
        if (Objects.equals(tvSeriesProvider, "TMDB")) {                     // Get values if TMDB.
            Log.d(TAG, "onCreate: Starting to get json.");
            String url = "https://api.themoviedb.org/3/tv/" + tvSeriesId + "?api_key=" + MainActivity.apiKeyTMDB + "&language=" + language;
            new GetJSON(this, url, "tv").execute();
            Log.d(TAG, "onCreate: Started GetJSON");
        }
        else {                                                              // Get values if TVDB
            TheTvdb theTvdb = new TheTvdb(MainActivity.apiKeyTVDB);
            retrofit2.Response<SeriesResponse> response = null;
            try {
                response = theTvdb.series()
                        .series(Integer.parseInt(tvSeriesId), language)
                        .execute();
            } catch (IOException e) {
                e.printStackTrace();
            }

            assert response != null;
            if (response.isSuccessful()) {
                Log.d(TAG, "onCreate: Shall show this tv-show from TVDB with id +" + tvSeriesId);
                Series series = response.body().data;
                TextView tvId = (TextView) findViewById(R.id.tv_id);
                TextView tvTitle = (TextView) findViewById(R.id.tv_title);
                TextView tvTitleOriginal = (TextView) findViewById(R.id.tv_title_original);
                TextView tvReleaseDate = (TextView) findViewById(R.id.tv_release_date);
                ImageView poster = (ImageView) findViewById(R.id.iv_poster);
                TextView tvOverview = (TextView) findViewById(R.id.tv_overview);

                tvId.setText(series.id);
                tvTitle.setText(series.seriesName);
                tvTitleOriginal.setText("");                        // Can't get, set to blank.
                tvReleaseDate.setText(series.firstAired);
                tvOverview.setText(series.overview);
                Log.d(TAG,series.seriesName + " is written!");
            }
        }
        db = FirebaseFirestore.getInstance();
        dbAuth = FirebaseAuth.getInstance();

        checkFavorite();

        // Listen for new comments
        db.collection("tvSeries" + tvSeriesProvider).document(tvSeriesId).collection("comments").orderBy("messageTime")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        // Get comments and write them out in the list:
                        List<ChatMessage> comments = new ArrayList<ChatMessage>();
                        for (QueryDocumentSnapshot document : value) {
                            Log.d(TAG, document.getId() + " => " + document.getData());
                            ChatMessage comment = document.toObject(ChatMessage.class);
                            comments.add(comment);
                        }
                        new ShowChatMessage(TVseriesInfo.this,comments).execute();
                    }
                });
    }

    public void sendComment(View view) {
        // Get comment:
        EditText text = (EditText)findViewById(R.id.et_text);

        // Create a new comment:
        ChatMessage comment = new ChatMessage(text.getText().toString(), dbAuth.getCurrentUser().getDisplayName(), dbAuth.getCurrentUser().getUid());

        // Add a new document with a generated ID
        db.collection("tvSeries" + tvSeriesProvider).document(tvSeriesId).collection("comments")
                .add(comment)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "Comment to " + tvSeriesId + " added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding comment", e);
                    }
                });

    }

    public void favorite(View view) {
        //Make addable item with generated id:
        Favorite data = new Favorite();
        data.setFavoriteId(tvSeriesId);
        data.setUserId(dbAuth.getCurrentUser().getUid());
        data.setUsername(dbAuth.getCurrentUser().getDisplayName());

        // Add a new document with a generated ID
        db.collection("user").document(dbAuth.getCurrentUser().getUid()).collection("favorites_tvSeries" + tvSeriesProvider)
                .add(data)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "User " + dbAuth.getCurrentUser().getDisplayName()  + " has set " + tvSeriesId + " as favorite (added with ID: " + documentReference.getId() + ")");
                        Button bt_favorite = (Button) findViewById(R.id.bt_favorite);
                        bt_favorite.setText("Added as favorite");
                        bt_favorite.setEnabled(false);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding as favorite", e);
                    }
                });

        db.collection("tvSeries" + tvSeriesProvider).document(tvSeriesId).collection("favorites")
                .add(data)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "User " + dbAuth.getCurrentUser().getDisplayName()  + " has set " + tvSeriesId + " as favorite (added with ID: " + documentReference.getId() + ")");
                        Button bt_favorite = (Button) findViewById(R.id.bt_favorite);
                        bt_favorite.setText("Added as favorite");
                        bt_favorite.setEnabled(false);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding as favorite", e);
                    }
                });
    }

    // Check if the user has favorited it.
    public void checkFavorite() {
        db.collection("user").document(dbAuth.getCurrentUser().getUid()).collection("favorites_tvSeries" + tvSeriesProvider)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Favorite favorite = document.toObject(Favorite.class);
                                if (Objects.equals(favorite.getFavoriteId(), tvSeriesId)) {
                                    Button bt_favorite = (Button) findViewById(R.id.bt_favorite);
                                    bt_favorite.setText("Added as favorite");
                                    bt_favorite.setEnabled(false);
                                }
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }
}
