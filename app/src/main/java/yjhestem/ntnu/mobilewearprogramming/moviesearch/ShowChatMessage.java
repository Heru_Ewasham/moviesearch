package yjhestem.ntnu.mobilewearprogramming.moviesearch;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Created by Bruker on 08.02.2018.
 *
 * This use the gotted message-data and add the proper messages to listview. It shows chat-messages.
 */

class ShowChatMessage extends AsyncTask<String, Void, SimpleAdapter> {
    private static final String TAG = "GetData";
    //Context context;
    @SuppressLint("StaticFieldLeak")
    private Activity activity;
    private List<ChatMessage> chatMessages;
    private String user;                 //If just to get one user
//.... other attributes

    ShowChatMessage(Activity _activity, List<ChatMessage> chatMessages){

        this.activity = _activity;
        this.chatMessages = chatMessages;
        this.user = "";
//other initializations...

    }

    ShowChatMessage(Activity _activity, List<ChatMessage> chatMessages, String _user){

        this.activity = _activity;
        this.chatMessages = chatMessages;
        this.user = _user;
//other initializations...

    }


    @Override
    protected SimpleAdapter doInBackground(String... params) {
        Log.d("lab2", "doInBackground: Starting..");
        Log.d(TAG, "displayChatMessages: Starting..");
        List<HashMap<String, String>> messages = new ArrayList<HashMap<String,String>>();
        Log.d(TAG, "displayChatMessages: Starting to go through messages");
        for (int i = 0; i < chatMessages.size(); i++) {
            Log.d(TAG, "displayChatMessages: Starting to go through new message");
            Boolean shallShow = true;               // If this message shall be shown.
            Log.d(TAG, "doInBackground: User to show is (if nothing here, accept all): " + user);
            if (!Objects.equals(user, "") && !Objects.equals(user, chatMessages.get(i).getMessageUser())) {
                Log.d(TAG, "doInBackground: Shall not show this message, user who wrote this message is " + chatMessages.get(i).getMessageUser());
                shallShow = false;
            }
            if (shallShow) {
                Log.d(TAG, "doInBackground: Shall show this message");
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("username", chatMessages.get(i).getMessageUser());
                hm.put("text", chatMessages.get(i).getMessageText());
                hm.put("timestamp", chatMessages.get(i).getMessageTime());
                messages.add(hm);
            }
            Log.d(TAG, "displayChatMessages: Finished with this message");
        }
        Log.d(TAG, "displayChatMessages: Add messages to listview");

        // Keys used in Hashmap
        String[] from = { "username","text","timestamp"};

        // Ids of views in listview_layout
        int[] to = { R.id.message_user,R.id.message_text,R.id.message_time};

        Log.d(TAG, "displayChatMessages: Start new adapter");

        return new SimpleAdapter(activity.getBaseContext(), messages, R.layout.message, from, to);
    }

    @Override
    protected void onPostExecute(SimpleAdapter adapter) {
        ListView listOfMessages = (ListView)activity.findViewById(R.id.lv_messages);
        listOfMessages.setAdapter(adapter);
        Log.d(TAG, "onPostExecute: Added messages to listview.");
    }
}