package yjhestem.ntnu.mobilewearprogramming.moviesearch;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

//import android.app.Fragment;

/**
 * Created by Bruker on 04.03.2018.
 *
 * Fragment Two is the Friends-list.
 */

public class FragmentCredits extends Fragment {
    private static final String TAG = "FragmentTwo";

    public FragmentCredits() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_credits, container, false);
    }


}
