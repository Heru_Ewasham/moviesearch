package yjhestem.ntnu.mobilewearprogramming.moviesearch;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Movie;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import static com.firebase.ui.auth.AuthUI.getApplicationContext;

/**
 * Created by Bruker on 20.03.2018.
 *
 * Functions for parsing JSON is stored here.
 */

public class ParseJSON {
    private static final String TAG = "ParseJSON";

    // Parse movies from themoviedb.org
    public void parseMovieList(JSONObject json, Activity activity) throws JSONException {
        JSONArray movieList = json.getJSONArray("results");
        List<HashMap<String, String>> movies = new ArrayList<HashMap<String,String>>();

        // For every movie:
        for (int i=0; i < movieList.length(); i++)
        {
            try {
                JSONObject movieJSON = movieList.getJSONObject(i);
                // Pulling items from the array
                //String oneObjectsItem = movie.getString("STRINGNAMEinTHEarray");
                //String oneObjectsItem2 = movie.getString("anotherSTRINGNAMEINtheARRAY");
                Log.d(TAG, "displayMovieList: Starting to go through new movie");
                MovieInList movie = new MovieInList(movieJSON.getInt("id"), movieJSON.getString("title"));
                Log.d(TAG, "doInBackground: Shall show this message");
                HashMap<String, String> hm = new HashMap<String, String>();
                Log.d(TAG, "parseMovieList: id: " + movie.getId());
                Log.d(TAG, "parseMovieList: title: " + movie.getName());
                hm.put("id", Integer.toString(movie.getId()));
                hm.put("title", movie.getName());
                movies.add(hm);
                Log.d(TAG, "displayMovieList: Finished with this movie");
            } catch (JSONException e) {
                // Oops
            }
        }
        Log.d(TAG, "displayMovieList: Add movies to listview");

        // Keys used in Hashmap
        String[] from = {"id", "title"};

        // Ids of views in listview_layout
        int[] to = { R.id.movie_id, R.id.movie_name };

        Log.d(TAG, "displayChatMessages: Start new adapter");

        SimpleAdapter adapter = new SimpleAdapter(activity.getBaseContext(), movies, R.layout.movie_in_list, from, to);
        ListView listOfMovies;
        listOfMovies = (ListView)activity.findViewById(R.id.lv_movieTMDB);
        listOfMovies.setAdapter(adapter);
        Log.d(TAG, "onPostExecute: Added movies to listview.");
    }

    // Parse movie from themoviedb.org
    public void parseMovie(JSONObject json, Activity activity) {
            try {
                // Pulling items from the array
                //String oneObjectsItem = movie.getString("STRINGNAMEinTHEarray");
                //String oneObjectsItem2 = movie.getString("anotherSTRINGNAMEINtheARRAY");
                MovieTMDB movie = new MovieTMDB(json.getInt("id"),
                        json.getString("title"),
                        json.getString("original_title"),
                        json.getString("overview"),
                        json.getBoolean("video"),
                        json.getString("release_date"),
                        json.getString("poster_path"));
                Log.d(TAG, "doInBackground: Shall show this movie, get all textviews");

                // Get and set info directly inside each element:
                TextView tvId = (TextView)activity.findViewById(R.id.tv_id);
                TextView tvTitle = (TextView)activity.findViewById(R.id.tv_title);
                TextView tvTitleOriginal = (TextView)activity.findViewById(R.id.tv_title_original);
                TextView tvReleaseDate = (TextView)activity.findViewById(R.id.tv_release_date);
                TextView tvIsVideo = (TextView)activity.findViewById(R.id.tv_isVideo);
                TextView tvOverview = (TextView)activity.findViewById(R.id.tv_overview);
                ImageView poster = (ImageView) activity.findViewById(R.id.iv_poster);
                Log.d(TAG, "parseMovie: id: " + movie.getId());
                Log.d(TAG, "parseMovie: title: " + movie.getTitle());
                tvId.setText(Integer.toString(movie.getId()));
                tvTitle.setText(movie.getTitle());
                tvTitleOriginal.setText(movie.getTitle_original());
                tvOverview.setText(movie.getOverview());
                tvIsVideo.setText(String.valueOf(movie.getVideo()));

                Log.d(TAG, "parseMovie: Adding image");
                new GetImage(poster).execute("https://image.tmdb.org/t/p/w300" + movie.getPoster());

                tvReleaseDate.setText(movie.getRelease_date());
                Log.d(TAG, "displayMovie: Finished with this movie");
            } catch (JSONException e) {
                // Oops
            }
        Log.d(TAG, "displayMovie: Add movie to listview");

        Log.d(TAG, "onPostExecute: Added movies to listview.");
    }

    //Parse TMDB tv list:
    public void parseTVList(JSONObject json, Activity activity) throws JSONException {
        JSONArray tvList = json.getJSONArray("results");
        List<HashMap<String, String>> tvSeries = new ArrayList<HashMap<String,String>>();

        // For every TV-series:
        for (int i=0; i < tvList.length(); i++)
        {
            try {
                JSONObject tvJSON = tvList.getJSONObject(i);
                // Pulling items from the array
                //String oneObjectsItem = movie.getString("STRINGNAMEinTHEarray");
                //String oneObjectsItem2 = movie.getString("anotherSTRINGNAMEINtheARRAY");
                Log.d(TAG, "displayMovieList: Starting to go through new tvSeries");
                TVSeriesInList tv = new TVSeriesInList(
                        tvJSON.getInt("id"),
                        tvJSON.getString("name"));
                HashMap<String, String> hm = new HashMap<String, String>();
                Log.d(TAG, "parseTVList: id: " + tv.getId());
                Log.d(TAG, "parseTVList: title: " + tv.getName());
                hm.put("id", Integer.toString(tv.getId()));
                hm.put("title", tv.getName());
                tvSeries.add(hm);
                Log.d(TAG, "displayMovieList: Finished with this tvSeries");
            } catch (JSONException e) {
                // Oops
            }
        }
        Log.d(TAG, "displayMovieList: Add movies to listview");

        // Keys used in Hashmap
        String[] from = {"id", "title"};

        // Ids of views in listview_layout
        int[] to = { R.id.movie_id, R.id.movie_name };

        Log.d(TAG, "displayChatMessages: Start new adapter");

        SimpleAdapter adapter = new SimpleAdapter(activity.getBaseContext(), tvSeries, R.layout.movie_in_list, from, to);
        ListView listOfMessages = (ListView)activity.findViewById(R.id.lv_tv_TMDB);
        listOfMessages.setAdapter(adapter);
        Log.d(TAG, "onPostExecute: Added movies to listview.");
    }

    // Parse a TV-series:
    public void parseTV(JSONObject json, Activity activity) {
        try {
            // Pulling items from the array
            //String oneObjectsItem = movie.getString("STRINGNAMEinTHEarray");
            //String oneObjectsItem2 = movie.getString("anotherSTRINGNAMEINtheARRAY");
            TVSeries tv = new TVSeries(json.getInt("id"),
                    json.getString("name"),
                    json.getString("original_name"),
                    json.getString("overview"),
                    json.getString("first_air_date"),
                    json.getString("poster_path"));
            Log.d(TAG, "doInBackground: Shall show this movie, get all textviews");

            // Get and set info directly inside each element.
            TextView tvId = (TextView)activity.findViewById(R.id.tv_id);
            TextView tvTitle = (TextView)activity.findViewById(R.id.tv_title);
            TextView tvTitleOriginal = (TextView)activity.findViewById(R.id.tv_title_original);
            TextView tvReleaseDate = (TextView)activity.findViewById(R.id.tv_release_date);
            ImageView poster = (ImageView) activity.findViewById(R.id.iv_poster);
            TextView tvOverview = (TextView)activity.findViewById(R.id.tv_overview);
            Log.d(TAG, "parseTV: id: " + tv.getId());
            Log.d(TAG, "parseTV: title: " + tv.getTitle());
            Log.d(TAG, "parseTV: overview" + json.getString("overview"));
            tvId.setText(Integer.toString(tv.getId()));
            tvTitle.setText(tv.getTitle());
            tvTitleOriginal.setText(tv.getTitle_original());
            tvOverview.setText(tv.getOverview());

            Log.d(TAG, "parseTV: Adding image");
            new GetImage(poster).execute("https://image.tmdb.org/t/p/w300" + tv.getPoster());

            tvReleaseDate.setText(tv.getRelease_date());
            Log.d(TAG, "parseTV: Finished with this TV-show");
        } catch (JSONException e) {
            // Oops
        //} catch (MalformedURLException e) {
        //    e.printStackTrace();
        //} catch (IOException e) {
        //    e.printStackTrace();
        }
        Log.d(TAG, "displayMovie: Add tv to listview");

        Log.d(TAG, "onPostExecute: Added tv-series to listview.");
    }

    //Parse TMDB language-list:
    public void parseLanguageList(JSONArray json, Activity activity) throws JSONException {

        //Create and fill an ArrayAdapter with a bunch of "Language" objects
        List<Language> languages = new ArrayList<Language>();;

        // For every language in json.
        for (int i=0; i < json.length(); i++)
        {
            Log.d(TAG, "parseLanguageList: Add new language");
            JSONObject language = json.getJSONObject(i);
            Language lang = new Language(language.getString("name"), language.getString("english_name"), language.getString("iso_639_1"));
            languages.add(lang);
            Log.d(TAG, "parseLanguageList: Added language " + lang.toString());
        }
        Log.d(TAG, "parseLanguageList: add languages to spinner");

        Spinner spinner = (Spinner) activity.findViewById(R.id.sp_update_language);

        ArrayAdapter<Language> languageAdapter = new ArrayAdapter<Language>(activity, android.R.layout.simple_spinner_item,languages);
        languageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(languageAdapter);
        Log.d(TAG, "onPostExecute: Added languages to spinner.");
    }

    //Parse TMDB movie previews
    public void parsePreviews(JSONObject json, Activity activity) throws JSONException {

        JSONArray previews = json.getJSONArray("results");
        List<HashMap<String, String>> previewList = new ArrayList<HashMap<String,String>>();

        // For every previews in json.
        for (int i=0; i < previews.length(); i++)
        {
            Log.d(TAG, "parsePreviews: Add new preview");
            JSONObject jsonPreview = previews.getJSONObject(i);
            Preview preview = new Preview(
                    jsonPreview.getString("id"),
                    jsonPreview.getString("name"),
                    jsonPreview.getString("key"),
                    jsonPreview.getString("site"),
                    jsonPreview.getInt("size"),
                    jsonPreview.getString("type"),
                    jsonPreview.getString("iso_639_1"),
                    jsonPreview.getString("iso_3166_1"));
            HashMap<String, String> hm = new HashMap<String, String>();
            Log.d(TAG, "parsePreviews: id: " + preview.getId());
            Log.d(TAG, "parsePreviews: name: " + preview.getName());
            Log.d(TAG, "parsePreviews: key: " + preview.getKey());
            Log.d(TAG, "parsePreviews: site: " + preview.getSite());
            if (Objects.equals(preview.getSite(), "YouTube")) {                       // Only youtube videos supported.
                Log.d(TAG, "parsePreviews: Preview is from YouTube, add to listview");
                hm.put("id", preview.getId());
                hm.put("name", preview.getName());
                hm.put("key", preview.getKey());
                hm.put("type", preview.getType());
                hm.put("language", preview.getIso_639_1() + "-" + preview.getIso_3166_1());
                previewList.add(hm);
                Log.d(TAG, "parsePreviews: Added preview");
            }
            else {
                Log.d(TAG, "parsePreviews: Preview not from YouTube, jump over this...");
            }
        }
        Log.d(TAG, "parsePreviews: Add previews to listview");

        // Keys used in Hashmap
        String[] from = {"id", "name", "key", "type", "language"};

        // Ids of views in listview_layout
        int[] to = { R.id.preview_id, R.id.preview_name, R.id.preview_key, R.id.preview_type, R.id.preview_language };

        Log.d(TAG, "displayChatMessages: Start new adapter");

        SimpleAdapter adapter = new SimpleAdapter(activity.getBaseContext(), previewList, R.layout.preview_list, from, to);
        ListView listOfMessages = (ListView)activity.findViewById(R.id.lv_previews);
        listOfMessages.setAdapter(adapter);
        Log.d(TAG, "onPostExecute: Added movies to listview.");
    }


}
