package yjhestem.ntnu.mobilewearprogramming.moviesearch;

/**
 * Created by Bruker on 04.03.2018.
 *
 * This store information about a TV-series.
 */

public class TVSeries {

    private int id;
    private String title;
    private String title_original;
    private String overview;
    private String poster_path;
    private String release_date;

    TVSeries(int id, String title, String title_original, String overview, String release_date, String poster_path) {
        this.id = id;
        this.title = title;
        this.title_original = title_original;
        this.overview = overview;
        this.poster_path = poster_path;
        this.release_date = release_date;
    }

    public TVSeries(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    String getTitle_original() {
        return title_original;
    }

    public void setTitle_original(String title_original) {
        this.title_original = title_original;
    }

    String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    String getPoster() {
        return poster_path;
    }

    public void setPoster(String poster) {
        this.poster_path = poster;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }
}
