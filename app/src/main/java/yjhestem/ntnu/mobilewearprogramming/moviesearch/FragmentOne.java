package yjhestem.ntnu.mobilewearprogramming.moviesearch;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

//import android.app.Fragment;

/**
 * Created by Bruker on 04.03.2018.
 *
 * Fragment one is the chat-message-list.
 */

public class FragmentOne extends Fragment {
    String TAG = "FragmentOne";

    public FragmentOne() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_one, container, false);

        //Onclick: show movie-info:
        ListView movieList = (ListView)view.findViewById(R.id.lv_movieTMDB);
        movieList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view.findViewById(R.id.movie_id);
                String movieId = textView.getText().toString();
                Intent intent = new Intent(view.getContext(), MovieInfo.class);
                Log.d(TAG, "On item click: Movie id to show: " + movieId);
                intent.putExtra("movieId", movieId);
                startActivity(intent);
            }
        });
        return view;
    }
}
