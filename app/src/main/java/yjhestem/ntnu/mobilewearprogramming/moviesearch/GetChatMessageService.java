package yjhestem.ntnu.mobilewearprogramming.moviesearch;

import android.app.Activity;
import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class GetChatMessageService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FOO = "yjhestem.ntnu.mobilewearprogramming.moviesearch.action.CheckNew";
    private static final String ACTION_BAZ = "yjhestem.ntnu.mobilewearprogramming.moviesearch.action.BAZ";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "yjhestem.ntnu.mobilewearprogramming.moviesearch.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "yjhestem.ntnu.mobilewearprogramming.moviesearch.extra.PARAM2";
    private static final String TAG = "GetChatMessageService";
    public static boolean isIntentServiceRunning = false;

    public GetChatMessageService() {
        super("GetChatMessageService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionCheckNew(Context context, String param1, String param2) {
        Intent intent = new Intent(context, GetChatMessageService.class);
        intent.setAction(ACTION_FOO);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionBaz(Context context, String param1, String param2) {
        Intent intent = new Intent(context, GetChatMessageService.class);
        intent.setAction(ACTION_BAZ);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        //If not already running, run (is found this solution to the problem here: https://stackoverflow.com/questions/28298696/android-how-to-determine-if-intentservice-is-running/28318719
        if(!isIntentServiceRunning) {
            isIntentServiceRunning = true;
            if (intent != null) {
                final String action = intent.getAction();
                if (ACTION_FOO.equals(action)) {
                    final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                    final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                    handleActionCheckNew(param1, param2);
                } else if (ACTION_BAZ.equals(action)) {
                    final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                    final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                    handleActionBaz(param1, param2);
                }
            }
        }
    }

    public void showNotification(String title, String content, Intent intent) {
        Log.d(TAG, "Sending notification titled " + title);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default",
                    "Moviesearch",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("Notificationchannel for my movie-search-app");
            assert mNotificationManager != null;
            mNotificationManager.createNotificationChannel(channel);
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), "default")
                .setSmallIcon(R.mipmap.ic_launcher) // notification icon
                .setContentTitle(title) // title for notification
                .setContentText(content)// message for notification
                //.setSound(R.raw.Siren) // set alarm sound for notification
                .setAutoCancel(true); // clear notification after click

        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pi);
        mNotificationManager.notify(0, mBuilder.build());
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionCheckNew(String param1, String param2) {
        // Access a Cloud Firestore instance.
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        final FirebaseAuth dbAuth = FirebaseAuth.getInstance();

        Log.d(TAG, "handleActionCheckNew: Started..");
        Log.d(TAG, "handleActionCheckNew: Starting to set up all listeners..");

        // Get all favorites current user has in TMDB-movies-part:
        db.collection("user").document(dbAuth.getCurrentUser().getUid()).collection("favorites_moviesTMDB")       //Listen for any changes in Favorites in "favorites_moviesTMDB".
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        // For every favorite in movie-TMDB, add a listener:
                        for (QueryDocumentSnapshot document : value) {                                                 // For every favorite.
                            Log.d(TAG, document.getId() + " => " + document.getData());
                            final Favorite favorite = document.toObject(Favorite.class);
                            db.collection("moviesTMDB").document(favorite.getFavoriteId()).collection("comments")   // Listen for new comments on every favorite.
                                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                        @Override
                                        public void onEvent(@Nullable QuerySnapshot value,
                                                            @Nullable FirebaseFirestoreException e) {
                                            if (e != null) {
                                                Log.w(TAG, "Listen failed.", e);
                                                return;
                                            }

                                            //Make an intent to go directly to the app + show notification.
                                            Intent intent = new Intent(getApplicationContext(), MovieInfo.class);
                                            intent.putExtra("movieId", favorite.getFavoriteId());
                                            showNotification("New comment added", "A new comment is added to one of your favorites", intent);

                                            //List<ChatMessage> comments = new ArrayList<ChatMessage>();
                                            /*for (QueryDocumentSnapshot document : value) {
                                                Log.d(TAG, document.getId() + " => " + document.getData());
                                                ChatMessage comment = document.toObject(ChatMessage.class);
                                                comments.add(comment);
                                            }*/
                                        }
                                    });
                        }
                    }
                });

        // Go through every favorite every user has in tv-series-TMDB-part:
        db.collection("user").document(dbAuth.getCurrentUser().getUid()).collection("favorites_tvSeriesTMDB")       //Listen for any changes in Favorites in "favorites_tvSeriesTMDB".
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        //For every favorite in TMDB-tv_series, add listener:
                        for (QueryDocumentSnapshot document : value) {                                                 // For every favorite.
                            Log.d(TAG, document.getId() + " => " + document.getData());
                            final Favorite favorite = document.toObject(Favorite.class);

                            db.collection("tvSeriesTMDB").document(favorite.getFavoriteId()).collection("comments")   // Listen for new comments on every favorite.
                                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                        @Override
                                        public void onEvent(@Nullable QuerySnapshot value,
                                                            @Nullable FirebaseFirestoreException e) {
                                            if (e != null) {
                                                Log.w(TAG, "Listen failed.", e);
                                                return;
                                            }

                                            //Make an intent to go directly to the app + show notification:
                                            Intent intent = new Intent(getApplicationContext(), MovieInfo.class);
                                            intent.putExtra("movieId", favorite.getFavoriteId());
                                            showNotification("New comment added", "A new comment is added to one of your favorites", intent);

                                            //List<ChatMessage> comments = new ArrayList<ChatMessage>();
                                            /*for (QueryDocumentSnapshot document : value) {
                                                Log.d(TAG, document.getId() + " => " + document.getData());
                                                ChatMessage comment = document.toObject(ChatMessage.class);
                                                comments.add(comment);
                                            }*/
                                        }
                                    });
                        }
                    }
                });
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBaz(String param1, String param2) {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        isIntentServiceRunning = false;
    }
}
