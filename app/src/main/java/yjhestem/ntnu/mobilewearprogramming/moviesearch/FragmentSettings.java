package yjhestem.ntnu.mobilewearprogramming.moviesearch;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

//import android.app.Fragment;

/**
 * Created by Bruker on 04.03.2018.
 *
 * Fragment Two is the Friends-list.
 */

public class FragmentSettings extends Fragment {
    private static final String TAG = "FragmentSettings";

    public FragmentSettings() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        //Get languages supported by TMDB:
        new GetJSON(getActivity(), "https://api.themoviedb.org/3/configuration/languages?api_key=" + MainActivity.apiKeyTMDB, "language").execute();

        return view;
    }


}
