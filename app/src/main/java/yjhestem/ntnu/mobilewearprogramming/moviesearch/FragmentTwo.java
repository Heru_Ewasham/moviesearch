package yjhestem.ntnu.mobilewearprogramming.moviesearch;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

//import android.app.Fragment;

/**
 * Created by Bruker on 04.03.2018.
 *
 * Fragment Two is the Friends-list.
 */

public class FragmentTwo extends Fragment {
    private static final String TAG = "FragmentTwo";

    public FragmentTwo() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_two, container, false);
        ListView lvTMDB = (ListView)view.findViewById(R.id.lv_tv_TMDB);

        //Onclick: Show tv-series:
        lvTMDB.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view.findViewById(R.id.movie_id);
                String tvSeriesId = textView.getText().toString();
                Intent intent = new Intent(view.getContext(), TVseriesInfo.class);
                Log.d(TAG, "On item click: TV id to show: " + tvSeriesId + " from TMDB");
                intent.putExtra("tvSeriesId", tvSeriesId);
                intent.putExtra("tvSeriesProvider", "TMDB");
                startActivity(intent);
            }
        });

        return view;
    }


}
