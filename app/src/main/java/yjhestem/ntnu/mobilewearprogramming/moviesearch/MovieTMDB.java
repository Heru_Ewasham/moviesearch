package yjhestem.ntnu.mobilewearprogramming.moviesearch;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Bruker on 04.03.2018.
 *
 * This store information about a movie from TMDB.
 */

public class MovieTMDB {

    private int id;
    private String title;
    private String title_original;
    private String overview;
    private boolean video;
    private String poster_path;
    private String release_date;
    private String preview;             // The youtube id for the preview.

    public MovieTMDB(int id, String title, String title_original, String overview, Boolean video, String release_date, String poster_path) {
        this.id = id;
        this.title = title;
        this.title_original = title_original;
        this.overview = overview;
        this.video = video;
        this.poster_path = poster_path;
        this.release_date = release_date;
        this.preview = "";              // Initially not set;
    }

    public MovieTMDB(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle_original() {
        return title_original;
    }

    public void setTitle_original(String title_original) {
        this.title_original = title_original;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public boolean getVideo() {
        return video;
    }

    public void setVideo(boolean video) {
        this.video = video;
    }

    public String getPoster() {
        return poster_path;
    }

    public void setPoster(String poster) {
        this.poster_path = poster;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getPreview() {
        if (this.preview != "") {        // If it is a preview, return id.
            return this.preview;
        }
        else {                          // Else return null.
            return null;
        }
    }

    public void setPreview(String youtubeId) {
        this.preview = youtubeId;
    }
}
