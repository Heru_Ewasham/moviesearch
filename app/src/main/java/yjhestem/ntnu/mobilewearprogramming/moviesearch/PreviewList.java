package yjhestem.ntnu.mobilewearprogramming.moviesearch;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeStandalonePlayer;

public class PreviewList extends AppCompatActivity {
    private String TAG = "PreviewList";

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_list);

        Intent intent = getIntent();
        String movieId = intent.getStringExtra("movieId");
        String movieTitle = intent.getStringExtra("movieTitle");

        String url = "https://api.themoviedb.org/3/movie/" + movieId + "/videos?api_key=" + MainActivity.apiKeyTMDB;
        new GetJSON(this,url,"preview").execute();          // Get and set previews.
        Log.d(TAG, "onCreate: Started GetJSON");

        TextView title = (TextView)findViewById(R.id.tv_title);
        title.setText("Previews for " + movieTitle);


        // Set a listener who listens on a click and then start a new youtube-activity (show a youtube-video):
        ListView lvPreview = (ListView)findViewById(R.id.lv_previews);
        lvPreview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView previewKey = (TextView) view.findViewById(R.id.preview_key);
                Log.d(TAG, "On item click: Preview to show: " + previewKey.getText());
                Intent intent = YouTubeStandalonePlayer.createVideoIntent(PreviewList.this, MainActivity.apiKeyGoogle, previewKey.getText().toString());
                startActivity(intent);
            }
        });
    }
}
