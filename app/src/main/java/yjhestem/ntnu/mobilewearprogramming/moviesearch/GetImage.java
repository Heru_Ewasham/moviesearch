package yjhestem.ntnu.mobilewearprogramming.moviesearch;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;

/**
 * This class Get image from web, like the GetJSON do.
 *
 * The Parse-JSON use this class to show the images.
 */


// Gotten from: https://inducesmile.com/android-tutorials-for-nigerian-developer/android-load-image-from-url/
public class GetImage extends AsyncTask<String, Void, Bitmap> {
    @SuppressLint("StaticFieldLeak")
    private ImageView bmImage;
    String TAG = "GetImage";
    GetImage(ImageView bmImage) {
        this.bmImage = bmImage;
    }

    protected Bitmap doInBackground(String... urls) {
        Log.d(TAG, "doInBackground: Starting..");
        String pathToFile = urls[0];                            // Get URL.
        Log.d(TAG, "doInBackground: URL is " + pathToFile);
        Bitmap bitmap = null;
        try {
            InputStream in = new java.net.URL(pathToFile).openStream();
            bitmap = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return bitmap;
    }
    protected void onPostExecute(Bitmap result) {
        Log.d(TAG, "onPostExecute: Setting image bitmap.");
        bmImage.setImageBitmap(result);
        Log.d(TAG, "onPostExecute: Set image bitmap.");
    }
}
