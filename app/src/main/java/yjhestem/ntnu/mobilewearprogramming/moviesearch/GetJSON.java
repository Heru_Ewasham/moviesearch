package yjhestem.ntnu.mobilewearprogramming.moviesearch;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

/**
 * Created by Bruker on 20.03.2018.
 *
 * We Get's the content (JSON-api) of a webpage.
 */

public class GetJSON extends AsyncTask<URL, Integer, String> {

    @SuppressLint("StaticFieldLeak")
    private Activity activity;
    private String website;
    private String type;
    private String TAG = "GetJSON";
//.... other attributes

    GetJSON(Activity activity, String url, String type){
        Log.d(TAG, "GetJSON: Setting up variable-settings");
        this.activity = activity;
        this.website = url;
        this.type = type;
//other initializations...

    }

    protected String doInBackground(URL... urls) {
        Log.d(TAG, "doInBackground: Starting...");
        HttpURLConnection urlConnection = null;
        StringBuilder result = new StringBuilder();
        try {
            Log.d(TAG, "doInBackground: Starting to get URL");
            URL url = new URL(website);
            urlConnection = (HttpURLConnection) url.openConnection();

            int code = urlConnection.getResponseCode();

            Log.d(TAG, "doInBackground: Response: " + code);
            if(code==200){                                                                // Is it any good (OK) response?
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
                String line = "";

                while ((line = bufferedReader.readLine()) != null)
                    result.append(line);                                    // I am not sure, but I think this was the result that came after automatic fixing warning with +=)
                in.close();
            }
            Log.d(TAG, "doInBackground: Result: " + result);
            return result.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        finally {
            assert urlConnection != null;
            urlConnection.disconnect();
        }
        Log.d(TAG, "doInBackground: Result: " + result);
        return result.toString();
    }

    protected void onPostExecute(String result) {
        Log.d(TAG, "onPostExecute: " + result);
        try {
            //Based on which json we get we send different types of json (object or array to the parser).
            if(Objects.equals(type, "movieList")) {         // Get a list of movies.
                JSONObject json = new JSONObject(result);
                new ParseJSON().parseMovieList(json, activity);
            }
            else if(Objects.equals(type, "tvList")) {       // Get a list of tv-series.
                JSONObject json = new JSONObject(result);
                new ParseJSON().parseTVList(json, activity);
            }
            else if(Objects.equals(type, "movie")) {        // Get a movie.
                JSONObject json = new JSONObject(result);
                new ParseJSON().parseMovie(json, activity);
            }
            else if(Objects.equals(type, "tv")) {           // Get a tv-series.
                JSONObject json = new JSONObject(result);
                new ParseJSON().parseTV(json, activity);
            }
            else if(Objects.equals(type, "language")) {     // Get languages.
                JSONArray json = new JSONArray(result);
                new ParseJSON().parseLanguageList(json, activity);
            }
            else if(Objects.equals(type, "preview")) {      // Get previews (is only used on movies)
                JSONObject json = new JSONObject(result);
                new ParseJSON().parsePreviews(json, activity);
            }
            /*else if(Objects.equals(type, "favorites")) {
                JSONObject json = new JSONObject(result);
                new ParseJSON().favorites(json, activity);
            }*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

        super.onPostExecute(result);
    }
}
