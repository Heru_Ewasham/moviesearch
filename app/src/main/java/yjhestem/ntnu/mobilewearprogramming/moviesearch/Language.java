package yjhestem.ntnu.mobilewearprogramming.moviesearch;

/**
 * Created by Bruker on 06.04.2018.
 *
 * Save a language (both the language in english and in the original language (name)).
 */

public class Language {
    private String name;            // Name of language in that language.
    private String englishName;     // Name of language in english.
    private String code;            // Language code ("en" for english, "no" for norwegian, "nb" for norwegian bokmål, "nn" for norwegian nynorsk, etc.)

    Language(String name, String englishName, String code) {
        this.name = name;
        this.englishName = englishName;
        this.code = code;
    }

    Language() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {                      // Used for printing on-screen.
        return this.name + "/" + this.englishName;
    }
}
