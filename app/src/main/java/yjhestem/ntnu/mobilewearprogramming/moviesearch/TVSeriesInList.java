package yjhestem.ntnu.mobilewearprogramming.moviesearch;

/**
 * Created by Bruker on 04.03.2018.
 *
 * This store information about tvSeries in a list, which is different than Movie
 */

public class TVSeriesInList {

    private int id;
    private String name;

    TVSeriesInList(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public TVSeriesInList(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
