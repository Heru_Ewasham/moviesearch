package yjhestem.ntnu.mobilewearprogramming.moviesearch;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Created by Bruker on 08.02.2018.
 *
 * This use the gotted message-data and add the proper messages to listview.
 */

class GetFavorites extends AsyncTask<String, Void, SimpleAdapter> {
    private static final String TAG = "GetFavorites";
    //Context context;
    @SuppressLint("StaticFieldLeak")
    private Activity activity;
    private List<Favorite> favorites;
    private String api;                 //Which api/listview shall be used.
    private String language;            //Which language to use.
//.... other attributes

    GetFavorites(Activity _activity, List<Favorite> favorites, String api, String language){

        this.activity = _activity;
        this.favorites = favorites;
        this.api = api;
        this.language = language;
//other initializations...

    }


    @Override
    protected SimpleAdapter doInBackground(String... params) {
        Log.d(TAG, "doInBackgroundFavorite: Starting..");
        List<HashMap<String, String>> movieList = new ArrayList<HashMap<String,String>>();
        Log.d(TAG, "displayFavoritesMovie: Starting to go through favorites");
        for (int i = 0; i < favorites.size(); i++) {                                // Go through every movie (or tv-show) gotten in the favorites:
            Log.d(TAG, "displayFavoritesMovie: Starting to go through new movie/tv-show");

            String result = "";
            if (Objects.equals(api, "TMDBMovies")) {        // If movie from TMDB, get content here:
                result = getWebsite("https://api.themoviedb.org/3/movie/" + favorites.get(i).getFavoriteId() + "?api_key=" + MainActivity.apiKeyTMDB + "&language=" + language);
            }
            else if (Objects.equals(api, "TMDBtv")) { // If tv-series from TMDB, get content here:
                result = getWebsite("https://api.themoviedb.org/3/tv/" + favorites.get(i).getFavoriteId() + "?api_key=" + MainActivity.apiKeyTMDB + "&language=" + language);
            }

            // Get the needed result from api to an hashmap
            HashMap<String, String> hm = new HashMap<String, String>();
            try {
                JSONObject json = new JSONObject(result);
                if (Objects.equals(api, "TMDBMovies")) {
                    Log.d(TAG, "doInBackgroundFavorite: Shall show this Movie");
                    Log.d(TAG, "doInBackgroundFavorite: Id = " + json.getInt("id"));
                    Log.d(TAG, "doInBackgroundFavorite: Title = " + json.getString("title"));
                    hm.put("id", String.valueOf(json.getInt("id")));
                    hm.put("title", json.getString("title"));
                }
                else if (Objects.equals(api, "TMDBtv")) {
                    Log.d(TAG, "doInBackgroundFavorite: Shall show this TV-series");
                    Log.d(TAG, "doInBackgroundFavorite: Id = " + json.getInt("id"));
                    Log.d(TAG, "doInBackgroundFavorite: Title = " + json.getString("name"));
                    hm.put("id", String.valueOf(json.getInt("id")));
                    hm.put("title", json.getString("name"));
                }
                movieList.add(hm);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.d(TAG, "displayFavoritesMovie: Finished with this movie");
        }
        Log.d(TAG, "displayFavoritesMovie: Add movie to listview");

        // Keys used in Hashmap
        String[] from = { "id","title"};

        // Ids of views in listview_layout (use "movie_in_list" for both movie and tv-series)
        int[] to = { R.id.movie_id,R.id.movie_name};

        Log.d(TAG, "displayChatMessages: Start new adapter");
        return new SimpleAdapter(activity.getBaseContext(), movieList, R.layout.movie_in_list, from, to);
    }

    @Override
    protected void onPostExecute(SimpleAdapter adapter) {
        ListView listView;
        // Choose which listview to "post" the list in (is dependent on which api is used):
        if (Objects.equals(api, "TMDBMovies")) {                              // TMDB movies.
            listView = (ListView)activity.findViewById(R.id.lv_TMDBMovies);
        }
        else {                                                                   // Default, but is TMDB tv-series.
            listView = (ListView)activity.findViewById(R.id.lv_TMDBTVSeries);
        }
        listView.setAdapter(adapter);
        Log.d(TAG, "onPostExecute: Added movies to listview.");
    }

    // Get result from api:
    private String getWebsite(String website) {
        Log.d(TAG, "doInBackground: Starting...");
        HttpURLConnection urlConnection = null;
        String result = "";
        try {
            Log.d(TAG, "doInBackground: Starting to get URL");
            URL url = new URL(website);
            urlConnection = (HttpURLConnection) url.openConnection();

            int code = urlConnection.getResponseCode();

            Log.d(TAG, "doInBackground: Response: " + code);
            if(code==200){                                          // If response OK:
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                if (in != null) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
                    String line = "";

                    while ((line = bufferedReader.readLine()) != null)
                        result += line;
                }
                in.close();
            }
            Log.d(TAG, "doInBackground: Result: " + result);
            return result;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        finally {
            urlConnection.disconnect();
        }
        Log.d(TAG, "doInBackground: Result: " + result);
        return result;
    }
}