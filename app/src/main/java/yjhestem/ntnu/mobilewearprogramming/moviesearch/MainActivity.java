package yjhestem.ntnu.mobilewearprogramming.moviesearch;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.uwetrottmann.thetvdb.entities.Series;
import com.uwetrottmann.thetvdb.entities.SeriesResponse;
import com.uwetrottmann.thetvdb.entities.SeriesResultsResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    //API-keys and default language, should maybe be somewhere else (saved securely), but after a quick search online the only result of constants was; save it where you will be using it most/fit most, and then get it in all other places by normal reference.
    public static String apiKeyTMDB = "6f952cdbe2a16f5d3cd79e9993590390";
    public static String apiKeyTVDB = "560CF4906F8BFCA0";
    public static String apiKeyGoogle = "AIzaSyCn65XvU8_W0WMFitb7mLfaEY1rBb0tdho";      // Used for youtube-api.
    public static String defaultLanguage = "en";
    public static int page = 1;                             // The  page number to show (the best results come at number one).
    public static boolean adultContent = false;             // Shall adult-content be shown (ie. can children use the app or not).

    private String TAG = "MainActivity";
    private static final int RC_SIGN_IN = 123;      // For firebase authentication intent
    private FirebaseAuth auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());


        // Add Fragments to adapter one by one
        adapter.addFragment(new FragmentOne(), "Movies");
        adapter.addFragment(new FragmentTwo(), "TV-series");
        adapter.addFragment(new FragmentFavorite(), "Favorite");
        adapter.addFragment(new FragmentSettings(), "Settings");
        adapter.addFragment(new FragmentCredits(), "Credits");
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);



        auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null) {
            GetChatMessageService.startActionCheckNew(this,"","");// Start a service to add eventlisteners, so whenewer a comment is added on s user-favorite, it will come a notification.
            // already signed in
            Toast.makeText(this,"Welcome back " + auth.getCurrentUser().getDisplayName(),Toast.LENGTH_LONG).show();
        } else {
            login();
        }



    }

    // Get info back from sign-in (check if signed-in or not):
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // RC_SIGN_IN is the request code you passed into startActivityForResult(...) when starting the sign in flow.
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            // Successfully signed in
            if (resultCode == RESULT_OK) {
                //startActivity(MainActivity.createIntent(this, response));
                //finish();
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {
                    GetChatMessageService.startActionCheckNew(this,"","");// Start a service to add eventlisteners, so whenewer a comment is added on s user-favorite, it will come a notification.
                    Toast.makeText(getApplicationContext(),"You are now signed in as " + user.getDisplayName(),Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(),"We are getting RESULT_OK, but not any user signed in yet.",Toast.LENGTH_SHORT).show();
                }
            } else {
                // Sign in failed
                if (response == null) {
                    // User pressed back button
                    Toast.makeText(getApplicationContext(), "You cancelled login, be aware that you then can't comment the media you are looking at",Toast.LENGTH_SHORT).show();
                    return;
                }

                if (response.getError().getErrorCode() == ErrorCodes.NO_NETWORK) {
                    Toast.makeText(getApplicationContext(), "No network connection, please try again later",Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(getApplicationContext(), "An error occured",Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Sign-in error: ", response.getError());
            }
        }
    }

    // Add Auth state listener in onStart method.
    @Override
    public void onStart() {
        super.onStart();
    }
    // release listener in onStop
    @Override
    public void onStop() {
        super.onStop();
    }

    public void searchMovies(View view) {
        // Get user-preferred language:
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final String language = prefs.getString("language",MainActivity.defaultLanguage);
        Log.d(TAG, "searchMovies: Language = " + language);
        Log.d(TAG, "onCreate: Starting searchString");
        EditText input = (EditText)findViewById(R.id.et_text);
        String searchString = null;
        try {
            searchString = URLEncoder.encode(input.getText().toString(), "UTF-8");      // URL-encode in UTF-8.
            Log.d(TAG, "onCreate: Searchstring encoded. Encoded string: " + searchString);
        } catch (UnsupportedEncodingException e) {
            Log.d(TAG, "onCreate: Searchstring encoding failed");
            e.printStackTrace();
        }
        Log.d(TAG, "onCreate: Starting to get json.");
        String url = "https://api.themoviedb.org/3/search/movie?api_key=" + MainActivity.apiKeyTMDB + "&language=" + language + "&query=" + searchString + "&page=" + MainActivity.page + "&include_adult=" + MainActivity.adultContent;
        new GetJSON(this,url,"movieList").execute();        // Get movies.
        Log.d(TAG, "onCreate: Started GetJSON");
    }

    /*
    Search after tv-series.
     */
    public void searchTVSeries(View view) {
        // Get user-preffered language:
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final String language = prefs.getString("language",MainActivity.defaultLanguage);
        Log.d(TAG, "onCreate: Starting searchString");
        EditText input = (EditText)findViewById(R.id.et_text_tv);
        String searchString = null;
        try {
            searchString = URLEncoder.encode(input.getText().toString(), "UTF-8");      // Encode search string so we can input it in as get
            Log.d(TAG, "onCreate: Searchstring encoded. Encoded string: " + searchString);
        } catch (UnsupportedEncodingException e) {
            Log.d(TAG, "onCreate: Searchstring encoding failed");
            e.printStackTrace();
        }
        Log.d(TAG, "onCreate: Starting to get json.");
        String url = "https://api.themoviedb.org/3/search/tv?api_key=" + MainActivity.apiKeyTMDB + "&language=" + language + "&query=" + searchString + "&page=" + MainActivity.page + "&include_adult=" + MainActivity.adultContent;
        new GetJSON(this,url,"tvList").execute();      // Get results and show them.
        Log.d(TAG, "onCreate: Started GetJSON");
    }

    public void updateSettings(View view) {
        Language language;
        Spinner spinner = (Spinner) findViewById(R.id.sp_update_language);

        Log.d(TAG, "updateSettings: Will update language");
        language = (Language) spinner.getSelectedItem();
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString("language", language.getCode());
        editor.apply();

        Log.d(TAG, "updateSettings: Have updated language to " + language.toString() + " (inserted code " + language.getCode() + " to preferences)");

        Toast.makeText(getApplicationContext(), "Settings is updated", Toast.LENGTH_SHORT).show();
    }

    public void login() {
        startActivityForResult(
                // Get an instance of AuthUI based on the default app
                AuthUI.getInstance().createSignInIntentBuilder().build(),
                RC_SIGN_IN);
    }

    // Adapter for the viewpager using FragmentPagerAdapter
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
