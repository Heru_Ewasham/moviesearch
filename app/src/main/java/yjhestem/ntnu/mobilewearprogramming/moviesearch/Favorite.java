package yjhestem.ntnu.mobilewearprogramming.moviesearch;

/**
 * Created by Bruker on 04.03.2018.
 *
 * This store information about tvSeries in a list, which is different than Movie
 */

public class Favorite {

    private String favoriteId;     // Id of the favorited item.
    private String userId;      // Id of user who favorited it.
    private String username;    // Username of the person who favorited it.


    public Favorite(String favoriteId, String userId, String username) {
        this.favoriteId = favoriteId;
        this.userId = userId;
        this.username = username;
    }

    Favorite(){

    }

    String getFavoriteId() {
        return favoriteId;
    }

    void setFavoriteId(String id) {
        this.favoriteId = id;
    }

    public String getUserId() {
        return userId;
    }

    void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    void setUsername(String username) {
        this.username = username;
    }
}
