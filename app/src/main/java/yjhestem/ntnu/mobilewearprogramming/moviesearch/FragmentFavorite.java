package yjhestem.ntnu.mobilewearprogramming.moviesearch;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import static com.firebase.ui.auth.AuthUI.getApplicationContext;

//import android.app.Fragment;

/**
 * Created by Bruker on 04.03.2018.
 *
 * Fragment Two is the Friends-list.
 */

public class FragmentFavorite extends Fragment {
    private static final String TAG = "FragmentFavorite";
    FirebaseFirestore db;
    FirebaseAuth dbAuth;

    public FragmentFavorite() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);


        db = FirebaseFirestore.getInstance();
        dbAuth = FirebaseAuth.getInstance();



        // Get favorites from the movie part of TMDB
        Task<QuerySnapshot> querySnapshotTask;
        querySnapshotTask = db.collection("user").document(dbAuth.getCurrentUser().getUid()).collection("favorites_moviesTMDB")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "onComplete: Getting TMDB movie favorites");
                            List<Favorite> favorites = new ArrayList<Favorite>();
                            for (QueryDocumentSnapshot document : task.getResult()) {           // For every TMDB movie, add it in a list.
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Favorite favorite = document.toObject(Favorite.class);

                                favorites.add(favorite);
                            }
                            // Get language and showw it.
                            final SharedPreferences prefs;
                            prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                            final String language = prefs.getString("language", MainActivity.defaultLanguage);
                            new GetFavorites(getActivity(), favorites, "TMDBMovies", language).execute();
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

        //Get favorites from the tv-part of TMDB.
        db.collection("user").document(dbAuth.getCurrentUser().getUid()).collection("favorites_tvSeriesTMDB")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "onComplete: Getting TMDB tv-series favorites");
                            List<Favorite> favorites = new ArrayList<Favorite>();
                            for (QueryDocumentSnapshot document : task.getResult()) {           // For every tv-favorite, show it.
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Favorite favorite = document.toObject(Favorite.class);

                                favorites.add(favorite);
                            }
                            final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                            final String language = prefs.getString("language",MainActivity.defaultLanguage);
                            new GetFavorites(getActivity(),favorites,"TMDBtv", language).execute();
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

        //-------------------------------------------------------------------------------

        // Onclick: Show movie/tv-show:

        ListView lvTMDBMovies = (ListView)view.findViewById(R.id.lv_TMDBMovies);
        lvTMDBMovies.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view.findViewById(R.id.movie_id);
                String tvSeriesId = textView.getText().toString();
                Intent intent = new Intent(view.getContext(), MovieInfo.class);
                Log.d(TAG, "On item click: Movie id to show: " + tvSeriesId + " from TMDB");
                intent.putExtra("movieId", tvSeriesId);
                intent.putExtra("MovieProvider", "TMDB");
                startActivity(intent);
            }
        });
        ListView lvTMDBTVSeries = (ListView)view.findViewById(R.id.lv_TMDBTVSeries);
        lvTMDBTVSeries.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view.findViewById(R.id.movie_id);
                String tvSeriesId = textView.getText().toString();
                Intent intent = new Intent(view.getContext(), TVseriesInfo.class);
                Log.d(TAG, "On item click: TV id to show: " + tvSeriesId + " from TMDB");
                intent.putExtra("tvSeriesId", tvSeriesId);
                intent.putExtra("tvSeriesProvider", "TMDB");
                startActivity(intent);
            }
        });
        return view;
    }


}
