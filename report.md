#Project report of Moviesearch:

Made by Yngve Hestem

##How I made it and some issues

I have mostly started with implementing movies and authentication (and the rest of firebase), and then going over to tv-series. See below for a list of how I made it and some problems I got.

* I used The movie database (TMDB)  without using any wrappers, but using JSON and Async task
* I used Firebase firestore for saving comments and favorites
* Used Firebase Authentication to make users for commenting and favorites
* Some small or general problems ofcourse, like adding notifications and services.
* Tried to implement The TV database (TVDB) with a wrapper (The bare api is more difficult at login), but got problems with wrapper and had no time to fix
* Are not a designer + had not much time, so the design is not good.
* I have some ui problems I didn't had time to fix (with this I mean that a few buttons are overlapping some text), plus  some updating (how often it updates).
* The TMDB-api returns empty string in the overview-field if not any overview-information in that language. This could be fixed by calling the api in english if this happens.

##Features:

* Show quick information (gotten from The Movie database (TMDB)
* Show previews for movies (like trailers) from youtube
* Commenting possible (app-specific)
* Set as favorite (app-specific)
* Get notification when someone add a comment on a favorited movie or tv-series
* Change languages gotten from api

##Development process:

I was alone, so therefore I just made it one step at a time. I first got the api-key to The Movie Database and then I started to write code around the same time.
I am not a person who write down plans, so I just made plans in my head (first I do this, then I do this).

My process of development was: Coding, testing, coding.

##Design:

I am not a designer, but a coder, but when I decided how the app should look, I thought about both how it was to code best and how it should look , and decided early that tv-series and movies should be seperated. Then I decided to use fragments as main-menu, which I used in Lab 4, as it would look good with such a design. Therefore I based the design upon the design I had on Lab 4 and added new Activity's and fragments on top of that.

The reason I used activities and not fragments as the base of movies/tv-series-info-page was just something I decided because I thought it whould look better when I decided it.

As I mentioned earlier, I am not a designer, and the current app has some design flaws. This should be fixed on before "production".

##ToDo

* Include The TV database
* Support more languages in ui (the change languages is just supported for the response gotten from api)?
* Adding other notifications
* Add more on settings (like adult-content)
* Logout from the app.
* Fix an "error" where the TMDB-api returns empty string on overview if not any overview in that language.
* Fix design flaws (see above)
* Take away more linter-warnings (see below)
* Comment better (see below)
* Structure the app-code a little bit better (see below)
* Structure the database a little better (see below)

More can be added.

##What was easy/hard and what did I learn

###Easy:
Using Firebase api and mostly adding json for TMDB (it was some small problems here, but not much).

###Hard:
Adding listeners for notification in services and trying to add TVDB (it was a reason I didn't add The TV database (TVDB) now). The reason TVDB api was hard was because it used a model where I don't just logged in first on one call to get another key, but needed to either log in again or, if inside 24 hours, call another script if I wanted it to go longer. This was a model I didn't had time to implement, and when I got problems with the wrapper library I found, I had no time to implement it in time.

###What have I learned:
I have learned that api's on the web can sometimes be easy (like TMDB was), but sometimes not (like TVDB).
I have also learned that I maybe should have used a wrapper library on TMDB also, so I had some more time on getting TVDB wrapper library to work.
I have learned how to deal with firebase and different api's (tried both TMDB (The movie Database) and TVDB (The TV Database).

##Testing:

I have just testet the app manually. It would have taken very much extra time to write extra testing code.

##Structure of app:

###Coding:

The app's coding structure is that every java-file is in the same folder. This could be structured better into sub-folders like "fragments", "api", etc.

The reason I have not done it was because I just started and then didn't had a clear plan on how to structure the code (I didn't have a clear overview of what I needed of code, so when the fragments came, I took that and thought about taking it in another folder, but decided not to, and when api-code came I was not sure how I should design the structure, but it was not that many files). The only java-code that are not in that folder is the youtube-api-code, which had a description on where I should put it in.

###Database:

The database used is Firebase Firestore.

At the moment the structure of the database is partly bad. The part who is bad is the part where you save usernames for comments. Because it is NoSQL you can't get both comments and username togheter if you only save username one place. At least not if I store username in a users-folder and movies/TV-shows (with comments) in another. If I call two Firebase-commands after each other and want to use info from both togheter, it would look bad. Therefore I have decided to, at the moment, store usernames inside each comment.

How database is structured (see example images in folder "database-structure"):

user - [[list of all users with their id]] - [[collections of favorited tv-series/movies]] - [[lists of favorited tv-series/movies]] - [[info]]

[[movie/tv-series from provider x (if make it bigger)]] - [[id of movie/tv-series]] - comments - [[lists of comments]] - [[comments-info]]

##Other things:

* It is still some linter-warnings. I have tried to do as best as I can to fix them, but some warnings will not go away automatically (with the automatic code-changer inside Android Studio), and some is very small. I will also point out that I some places (since you asked for it), has fixed some warnings where the automatic warnings-fixer write some not-so-good-readable code, but since you asked us to fix all warnings I will not always give any extra information about what is happening in-code even if it sounds strange (like for example how it handles += after warning-fixing). You are now warned.
* Some linter-warnings I may have missed, especially because when I have gotten trough all warnings I see in Android Studio (who it points out), I still may see that Android Studio say it still is some warnings, even though it is not showing up..
* I try to make good comments, but it may still be better comments. But I have used many logd-messages as comments when debugging, and this might function as comments now too I think.