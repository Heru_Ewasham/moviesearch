# Moviesearch #

Made by Yngve Hestem

This is an Android app made as a schoolproject. It uses TMDB (The Movie database) to show some quick info about movies and tv-shows. See list of features below.

You need to make an accont on the app to use it (is used to save your favorites and comments).

The report is written in the files "report.md" and "report.pdf" (which is a pdf-version of the md-file). I suggest the pdf for readabillity.

##Features

* Show quick information (gotten from The Movie database (TMDB)
* Show previews for movies (like trailers) from youtube
* Commenting possible (app-specific)
* Set as favorite (app-specific)
* Get notification when someone add a comment on a favorited movie or tv-series
* Change languages gotten from api

##Login

To use the app you need to make an accont and login to the app. The account is app-specific and therefore not connected to the TMDB library.

The only supported login-method is via email. The app uses Firebase Authentication to make accounts.

You will be asked to login/create account when you start the app for first time. You will not be able to logout at the moment, but it might be added later.

The account is at the moment only used to store information about who 

##Code structure:

The code is now structured in one folder for java-files (default folder), and one folder for XML-files (default folder).
It is also one executable java-package-file (JAR-file) with code for the Youtube-api placed under libs (which was told to do when downloading it from Google).

See report for more on code-structure.

##Website

https://yngvehestem.no/movie_search_chat/